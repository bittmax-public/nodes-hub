

OFFLINE NODE SETUP -

-----------------------------------------------------------------------------------------

STEP 1 - Make bitcoin.conf


datadir=/blockchain - This parameter will keep all blockchain related data inside "/blockchain folder". Feel free to change its value
testnet=0
server=1
rpcallowip=127.0.0.1
rpcuser=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
rpcpassword=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
maxconnections=0

You can keep this file wherever you want.

You can always see these param's description by running "./bitcoind -h" .

-----------------------------------------------------------------------------------------

STEP 2 - Run bitcoind.

  Run "./bitcoind -conf="bitcoin.conf file path"

  Now, test your setup that you are not listening for blockchain.

  Run "./bitcoin-cli -conf="bitcoin.conf file path" getblockchaininfo" and check verification progress.

--------------------------------------------------------------------------------------------

STEP 3 - Generate Address, (Public,Private Key) Pair 

  Run "./bitcoin-cli -conf=bitcoin.conf getnewaddress"

  This will generate a P2SH address.

----------------------------------------------------------------------------------------

STEP 4 - Secure your keys 

  Now, cd into the /blockchain directory, where blockchain data is present 
  and lookout for a file named "wallet.dat"

  Make multiple copies of this file and ensure that whenever you generate a new address, 
  you make copy of your file.

  Although, you can always use only master keys ( xpriv, xpub keys) for any recovery of your keys.
  But still, its a good practice to take this file's backup once in a while.

  Firewall Settings ( TODO ).

  Also, drop any public interfaces connected. There is no need for any public interface at all.

--------------------------------------------------------------------------------------

STEP 5 - Derive Public Key from your Private Key 

  RUN ./bitcoin-cli -conf=bitcoin.conf dumpwallet 1.txt

  Open 1.txt and search private key which corresponds to your address.

  You can confirm your formats here :-

  https://en.bitcoin.it/wiki/List_of_address_prefixes

  Private Key will be in WIF format.

  
  Go to this link - 
  https://segwitaddress.org/?testnet=true/#entropyRef

  In Details section , enter your private key and you will be able to see your public key and corresponing address.

  NOTE - Remove testnet=true in this url for generating mainnet keys

  IMPORTANT - Double Check that there is no HTTP call being made on the backend by this site when you visit this url.

------------------------------------------------------------------------------------------------------------

 
STEP 6 - Encrypt wallet.dat file and note down passphrase.


https://bitcoincore.org/en/doc/0.16.0/rpc/wallet/encryptwallet/



------------------------------------------------------------------------------------------------------------
