

OFFLINE NODE SETUP

----------------------------------------------------------------


There is no UTXO concept in Ethereum as it is account integer balanced based and 
ETH is just money in numbers like in banks rather than UTXO's.

So, no need to get public key and import it in other node.

Just generate private keys and note down your address and passphrase.

That should suffice.

"passphrase" - used to encrypt your private keys.

In ethereum , you need to set a passphrase for each address whilst generating it.


----------------------------------------------------------------------------


You can run geth using 

./geth --datadir "/blockchain" \
       --testnet \
       --networkid 3 \
       --syncmode "fast" \
       --cache 2048 \
       --rpc --rpcaddr "RPC_ADDRESS" --rpcapi "net,eth,web3,personal,txpool" \
       --ws --wsaddr "WS_ADDRESS" --wsapi "net,eth,web3,personal,txpool" --wsorigins "WS_ORIGINS"


NOTE :- Please run this command carefully and dont run it until you know what all the parameters are doing.


----------------------------------------------------------------------------------


FIREWALL SETTINGS

PRIVATISATION

-----------------------------------------------------------------------------------


CREATE NEW ACCOUNT 

./geth --datadir "/blockchain/" account new

You will be asked to enter a passphrase.

Role of passphrase is mentioned above.

Note down your address and passphrase.

You can always do "cd /blockchain" and find folders :-

- geth => Blockchain

- keystore => Contains private keys

------------------------------------------------------------------------------------


